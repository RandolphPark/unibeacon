//
//  EventVC.swift
//  UniBeacon
//
//  Created by Randolph Park on 22/02/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//

import UIKit
import CoreLocation


class EventVC: UIViewController {
    
    @IBOutlet weak var eventTitle: UILabel!
 
    @IBOutlet weak var eventTime: UILabel!
    
    @IBOutlet weak var eventLocation: UILabel!
    
    @IBOutlet weak var eventDescription: UILabel!
    
    @IBOutlet weak var conversationBTN: UIButton!
    @IBOutlet weak var peopleBTN: UIButton!
    @IBOutlet weak var eventCloudBTN: UIButton!
    var major:NSNumber = 0 
    
    var minor:NSNumber = 0
    var eventId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // mark: TEST
//        MyBeacon.major = 53893
//        MyBeacon.minor = 62643
        
        println("it is in the EventVC")
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "methodOfReceivedInternalNotificationIn:", name:"updateBeaconInformationToIn", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "methodOfReceivedInternalNotificationOut:", name:"updateBeaconInformationToOut", object: nil)
        updateDisplay()
    }
    @IBAction func refreshPage(sender: AnyObject) {
        updateDisplay()
        println("current major is \(self.major), current minor is \(self.minor)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        

    }
    func methodOfReceivedInternalNotificationIn(notification: NSNotification){
        //Action take on Notification
        println("get internal notification in")
        updateDisplay()
    }
    func methodOfReceivedInternalNotificationOut(notification: NSNotification){
        //Action take on Notification
        println("get internal notification out")
        updateDisplay()
    }
    
    func updateDisplay()->Void {
        
        if (MyBeacon.major == 0 && MyBeacon.minor == 0 ){
            resetContent()
        }
        
        self.major = MyBeacon.major
        self.minor = MyBeacon.minor
        
        println("update display major is \(MyBeacon.major), \(MyBeacon.minor)")
        
        let utcTimeZone = NSTimeZone(abbreviation: "UTC")
        let formatter = NSDateFormatter()
        let posIx = NSLocale(localeIdentifier: "en_AU")
        formatter.locale = posIx
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        formatter.timeZone = utcTimeZone
        let now = NSDate()
        let utcTimeString = formatter.stringFromDate(now)
        //println("before call PF Cloud major is \(self.major) and minor is  \(self.minor) current time is  \(utcTimeString)")
        PFCloud.callFunctionInBackground("searchByMinorMajorTime", withParameters: ["nowTime":utcTimeString, "major":self.major, "minor":self.minor]) {
            (response : AnyObject!, error : NSError!) -> Void in
            if error == nil && response != nil {
                println(response)
                //println("In the Event View --------  \(response)")
                if let event = response as? PFObject {
                    self.eventId = event.objectId
                    //self.navigationItem.title = event["eventName"] as? String
                    
                    self.eventTitle.text = event["eventName"] as? String
                    self.eventLocation.text = event["locationName"] as? String
                    self.eventDescription.text = event["eventDescription"] as? String
                    var timeString = ""
                    
                    let localDisplayFormatString = "dd/MM-HH:mm"
                    let localDisplayFormatter = NSDateFormatter()
                    localDisplayFormatter.locale = posIx
                    localDisplayFormatter.timeZone = NSTimeZone(name: "Australia/Brisbane")
                    localDisplayFormatter.dateFormat = localDisplayFormatString;
                    
                    if let fetchedStartTime = event["eventStartTime"] as? NSDate {
                        timeString = "\(localDisplayFormatter.stringFromDate(fetchedStartTime)) ~ "
                    }
                    
                    if let fetchedEndTime = event["eventEndTime"] as? NSDate {
                        timeString += "\(localDisplayFormatter.stringFromDate(fetchedEndTime))"
                    }
                    
                    self.eventTime.text = timeString
                    
                    self.eventCloudBTN.enabled = true
                    self.eventCloudBTN.alpha = 1.0
                    self.peopleBTN.enabled = true
                    self.peopleBTN.alpha = 1.0
                    self.conversationBTN.enabled = true
                    self.conversationBTN.alpha = 1.0
                    
                }
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "eventCloud" {
            var eventCloudTableVC: EventCloudTableVC = segue.destinationViewController as! EventCloudTableVC;
            eventCloudTableVC.eventId = self.eventId;
            
        } else  if segue.identifier == "conversationPressed" {
            var conversationTableVC: ConversationTableVC = segue.destinationViewController as! ConversationTableVC;
            conversationTableVC.eventId = self.eventId;
        } else if segue.identifier == "peopleLInkPressed" {
            var peopleTableVc: PeopleTableVC = segue.destinationViewController as! PeopleTableVC
            peopleTableVc.eventId = self.eventId
            
        }
    }
    
    func resetContent()->Void {
        self.eventTitle.text = "No Event Right Now"
        self.eventTime.text = ""
        self.eventLocation.text = ""
        self.eventDescription.text = ""
        
        self.eventCloudBTN.enabled = false
        self.eventCloudBTN.alpha = 0.4
        self.peopleBTN.enabled = false
        self.peopleBTN.alpha = 0.4
        self.conversationBTN.enabled = false
        self.conversationBTN.alpha = 0.4
    }
}

