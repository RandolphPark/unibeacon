//
//  AddEventVC.swift
//  UniBeacon
//
//  Created by Randolph Park on 23/03/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//

import Foundation
import UIKit

class AddEventVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate, UITextFieldDelegate,UIPickerViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var helper:TableViewHelper?
    
    var locationList = [PFObject]()
    
    override func viewDidLoad() {
        self.helper = TableViewHelper(tableView:tableView)
        
//        let cellIdentifier = "eventCreationTextInput"
//        let cell: EventCreationTextInputCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as EventCreationTextInputCell
//       
        configureTableView()
        
        // Section One
        var placeholder = NSAttributedString(string: "Event name...", attributes: [NSForegroundColorAttributeName : UIColor.lightGrayColor()])
        
        let eventTitleInput : EventCreationTextInputCell = self.tableView.dequeueReusableCellWithIdentifier("eventCreationTextInput")! as! EventCreationTextInputCell
        eventTitleInput.textInput.attributedPlaceholder = placeholder
        eventTitleInput.textInput.delegate = self
        self.helper?.addCell(0, cell:eventTitleInput, name: "eventTitle");
        
        // Section two
        placeholder = NSAttributedString(string: "Event description / note...", attributes: [NSForegroundColorAttributeName : UIColor.lightGrayColor()])
        let eventDescriptionInput : EventCreationTextInputCell = self.tableView.dequeueReusableCellWithIdentifier("eventCreationTextInput")! as! EventCreationTextInputCell
        eventDescriptionInput.textInput.delegate = self
        eventDescriptionInput.textInput.attributedPlaceholder = placeholder
        self.helper?.addCell(1, cell:eventDescriptionInput, name: "eventDescription");
        
        
        // section three
        let timeStartLabel : UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("eventCreationNormalCell")! as! UITableViewCell
        timeStartLabel.textLabel?.text = "Start Time"
        timeStartLabel.detailTextLabel?.text = ""
        self.helper?.addCell(2, cell: timeStartLabel , name: "timeStartLabel");

        let timeStartPicker : EventCreationDatePickerCell = self.tableView.dequeueReusableCellWithIdentifier("eventCreationDatePicker")! as! EventCreationDatePickerCell
        timeStartPicker.datePicker.addTarget(self, action: Selector("startDatePickerChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        self.helper?.addCell(2, cell: timeStartPicker , name: "timeStartPicker");
        
        
        let timeEndLabel : UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("eventCreationNormalCell")! as! UITableViewCell
        timeEndLabel.textLabel?.text = "End Time"
        timeEndLabel.detailTextLabel?.text = ""
        self.helper?.addCell(2, cell: timeEndLabel , name: "timeEndLabel");
        
        let timeEndPicker : EventCreationDatePickerCell = self.tableView.dequeueReusableCellWithIdentifier("eventCreationDatePicker")! as! EventCreationDatePickerCell
        timeEndPicker.datePicker.addTarget(self, action: Selector("endDatePickerChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        self.helper?.addCell(2, cell: timeEndPicker , name: "timeEndPicker");
    
        
        // section four
        let roomLabel : UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("eventCreationNormalCell")! as! UITableViewCell
        roomLabel.textLabel?.text = "Selected Room"
        roomLabel.detailTextLabel?.text = ""
        self.helper?.addCell(3, cell: roomLabel , name: "roomNameLabel");
        
        let roomPicker : EventCreationRoomPickerCell = self.tableView.dequeueReusableCellWithIdentifier("eventCreationRoomPicker")! as! EventCreationRoomPickerCell
        roomPicker.roomPicker.delegate = self
        
        self.helper?.addCell(3, cell: roomPicker, name: "roomPicker");
        
        
        // NOTE : hide some cells by default
        helper!.hideCell("timeStartPicker")
        helper!.hideCell("timeEndPicker")
        helper!.hideCell("roomPicker")

        var query = PFQuery(className:"iBeaconLocation")
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                // The find succeeded.
                println("Successfully retrieved \(objects) scores.")
                // Do something with the found objects
                if let objects = objects as? [PFObject] {
                    //println(objects)
                    self.locationList = objects
//                    for object in objects {
//                        println(object["locationName"])
//                    }
                }
            } else {
                // Log details of the failure
                println("Error: \(error) \(error.userInfo!)")
            }
        }

    }
    
    func configureTableView() {
        //println("i am configureing the table view ")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 120.0
    }
    
    func startDatePickerChanged(datePicker:UIDatePicker) {
        var dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        var strDate = dateFormatter.stringFromDate(datePicker.date)
        
        let index = self.helper?.indexPathForCellNamed("timeStartLabel")!
        self.helper?.cellForRowAtIndexPath(index!).detailTextLabel?.text = strDate
        
    }
    
    func endDatePickerChanged(datePicker:UIDatePicker) {
        var dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        var strDate = dateFormatter.stringFromDate(datePicker.date)
        
        let index = self.helper?.indexPathForCellNamed("timeEndLabel")!
        self.helper?.cellForRowAtIndexPath(index!).detailTextLabel?.text = strDate
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        let count = helper!.numberOfSections()
        return count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return helper!.numberOfRowsInSection(section)
    }
    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        if indexPath.section == 0 && indexPath.row == 1 {
//            return 193
//        }
//        
//        return tableView.rowHeight
//    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return helper!.cellForRowAtIndexPath(indexPath)
    }
    
    func tableView(tableView: UITableView,
        titleForHeaderInSection section: Int)
        -> String? {
            
            switch section
            {
            case 0:
                return "Event Name"
            case 1:
                return "Event Description"
            case 2:
                return "Event Start / End Time"
            case 3:
                return "Event Location"
            default:
                return "Error"
                //println("section err !!!")
            }
            
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if let name = helper!.cellNameAtIndexPath(indexPath) {
            switch name {
            case "timeStartLabel":
                if helper!.cellIsVisible("timeStartPicker") {
                    helper?.hideCell("timeStartPicker")
                } else {
                    self.foldAllPicker()
                    helper?.showCell("timeStartPicker")
                }
                // timeStartLabel  timeEndLabel  roomNameLabel
                //  timeStartPicker  timeEndPicker  roomPicker
            case "timeEndLabel":
                if helper!.cellIsVisible("timeEndPicker") {
                    helper?.hideCell("timeEndPicker")
                } else {
                    self.foldAllPicker()
                    helper?.showCell("timeEndPicker")
                }
                
            case "roomNameLabel":
                if helper!.cellIsVisible("roomPicker") {
                    helper?.hideCell("roomPicker")
                } else {
                    self.foldAllPicker()
                    helper?.showCell("roomPicker")
                }
                
            default:
                println("other cell pressed")
            }
            
        }
    }
    
    func foldAllPicker()-> Void {
        helper?.hideCell("timeStartPicker")
        helper?.hideCell("timeEndPicker")
        helper?.hideCell("roomPicker")
    }
    
    
    // ui picker view relaed 
    
    //let pickerData = ["Mozzarella","Gorgonzola","Provolone","Brie","Maytag Blue","Sharp Cheddar","Monterrey Jack","Stilton","Gouda","Goat Cheese", "Asiago"]
    
    
    
    //MARK: - UIPICKER Delegates and datasources
    //MARK: UIPICKER Data Sources
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return locationList.count
    }
    
    //MARK: UIPICKER Delegates
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        if let location:PFObject = locationList[row] as PFObject? {
            return location["locationName"] as! String
        } else {
            return ""
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        let index = self.helper?.indexPathForCellNamed("roomNameLabel")!
        
        if let location:PFObject = locationList[row] as PFObject? {
            self.helper?.cellForRowAtIndexPath(index!).detailTextLabel?.text = location["locationName"] as? String
        } else {
            self.helper?.cellForRowAtIndexPath(index!).detailTextLabel?.text = "err"
        }
        
        
    }
    
//    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
//        let titleData = pickerData[row]
//        var myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 15.0)!,NSForegroundColorAttributeName:UIColor.blueColor()])
//        return myTitle
//    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        var pickerLabel = view as! UILabel!
        if view == nil {  //if no label there yet
            pickerLabel = UILabel()
            
            //color  and center the label's background
            //let hue = CGFloat(row)/CGFloat(pickerData.count)
            //pickerLabel.backgroundColor = UIColor(hue: hue, saturation: 1.0, brightness:1.0, alpha: 1.0)
            pickerLabel.textAlignment = .Center
            
        }
        
        var titleData = "err"
        if let location:PFObject = locationList[row] as PFObject? {
            titleData = location["locationName"] as! String
        }
        
        
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 26.0)!,NSForegroundColorAttributeName:UIColor.blackColor()])
        pickerLabel!.attributedText = myTitle
        
        return pickerLabel
        
    }
    
        //size the components of the UIPickerView
//        func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
//            return 36.0
//        }
//    
//        func pickerView(pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
//            return 200
//        }
    

//
//    
//    @IBAction func showHideCell(sender: AnyObject) {
//        let button = sender as UIButton
//        let label = button.titleLabel!
//        let title = label.text!
//        
//        if helper!.cellIsVisible(title) {
//            helper!.hideCell(title)
//        } else {
//            helper!.showCell(title)
//        }
//    }
//    
    
    //MARK: TextField Delegate Function
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        println("text field entered return")
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func submitEvent(sender: AnyObject) {
        
        let indexTimeStart = self.helper?.indexPathForCellNamed("timeStartLabel")!
        let timeStartString = self.helper?.cellForRowAtIndexPath(indexTimeStart!).detailTextLabel?.text
        
        let indexTimeEnd = self.helper?.indexPathForCellNamed("timeEndLabel")!
        let timeEndString = self.helper?.cellForRowAtIndexPath(indexTimeEnd!).detailTextLabel?.text
        
        let indexRoomLabel = self.helper?.indexPathForCellNamed("roomNameLabel")!
        let locationString = self.helper?.cellForRowAtIndexPath(indexRoomLabel!).detailTextLabel?.text
        
        let eventTitleIndex = self.helper?.indexPathForCellNamed("eventTitle")!
        let titleCell : EventCreationTextInputCell = self.helper?.cellForRowAtIndexPath(eventTitleIndex!) as! EventCreationTextInputCell
        let eventTitleString = titleCell.textInput.text
        
        let eventDescriIndex = self.helper?.indexPathForCellNamed("eventDescription")!
        let descriptionCell : EventCreationTextInputCell = self.helper?.cellForRowAtIndexPath(eventDescriIndex!) as! EventCreationTextInputCell
        let eventDescriptionString = descriptionCell.textInput.text
        
        if (timeStartString == "" || timeEndString == "" || locationString == "" || eventTitleString == "" || eventDescriIndex == "") {
            let alert = UIAlertController(title: "Error", message: "Please fill up all the information", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
        } else {
            let location:PFObject = self.findLocationByName(locationString!)
            
            let UUID:String = location["UUID"] as! String
            let locationAddress:String = location["locationAddress"] as! String
            let major:NSNumber = location["major"] as! NSNumber
            let minor:NSNumber = location["minor"] as! NSNumber
 
            
            var dateFormatter = NSDateFormatter()
            
            dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
            dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
            dateFormatter.timeZone = NSTimeZone(name: "Australia/Brisbane")
//            let posIx = NSLocale(localeIdentifier: "en_AU")
//            dateFormatter.locale = posIx
            
            println(timeStartString)
            println(timeEndString)
            let startTime:NSDate = dateFormatter.dateFromString(timeStartString!)!
            let endTime : NSDate = dateFormatter.dateFromString(timeEndString!)!
            
            let hostName = PFUser.currentUser().username
           
            var ibeaconLocation  =  PFObject(className:"EventDetail")
            ibeaconLocation["eventHostName"] = hostName;
            ibeaconLocation["eventName"] = eventTitleString;
            ibeaconLocation["eventDescription"] = eventDescriptionString;
            ibeaconLocation["eventStartTime"] = startTime;
            ibeaconLocation["eventEndTime"] = endTime;
            ibeaconLocation["beaconUUID"] = UUID;
            ibeaconLocation["beaconMajor"] = major;
            ibeaconLocation["beaconMinor"] = minor;
            ibeaconLocation["eventName"] = eventTitleString;
            ibeaconLocation["locationAddress"] = locationAddress;
            ibeaconLocation["locationName"] = locationString;
            ibeaconLocation.save();
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        
    }
    
    func findLocationByName(locationName:String)->PFObject {
        for object in self.locationList {
            if locationName == object["locationName"] as? NSString {
                return object
            }
        }
        return PFObject()
    }
}

