//
//  AppDelegate.swift
//  UniBeacon
//
//  Created by Randolph Park on 20/02/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    //MARK: Dropbox delegate function
    func application(application: UIApplication, openURL url: NSURL,
        sourceApplication: String?, annotation: AnyObject?) -> Bool {
            
            if (DBChooser.defaultChooser().handleOpenURL(url)){
                
                return true;
            }
            
            return false
    }

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        Parse.enableLocalDatastore()
        Parse.setApplicationId("IjTUSDU7AFL4psBaJHhb6RKF01C1hAWT4t6Ctk3D", clientKey: "z9ZHSs8WClwfB6sbdSxp0fY9wdgPgPu41pp0dlZV")
        
        
        // navigation colour setting
        var navigationColour:UIColor = UIColor(red: 29/255.0, green: 85/255.0, blue: 146/255.0, alpha: 1.0)
        UINavigationBar.appearance().barTintColor = navigationColour;
        var navigationTintColour:UIColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 255.0/255.0)
        UINavigationBar.appearance().barStyle = UIBarStyle.Black
        UINavigationBar.appearance().tintColor = navigationTintColour;
        
        var shadow:NSShadow = NSShadow();
        shadow.shadowColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.8);
        shadow.shadowOffset = CGSizeMake(0, 1);
        
        
        UINavigationBar.appearance().titleTextAttributes = NSDictionary(objectsAndKeys:UIColor.whiteColor(), NSForegroundColorAttributeName, shadow, NSShadowAttributeName, UIFont(name: "HelveticaNeue-CondensedBlack", size: 21.0)!, NSFontAttributeName) as [NSObject : AnyObject]
//        UINavigationBar.appearance().titleTextAttributes = NSDictionary(objectsAndKeys:UIColor.whiteColor(), NSForegroundColorAttributeName, shadow, NSShadowAttributeName, UIFont(name: "HelveticaNeue-CondensedBlack", size: 21.0)!, NSFontAttributeName)           
//
//            [NSForegroundColorAttributeName: UIColor.whiteColor(), NSShadowAttributeName:UIFont(name: "HelveticaNeue-CondensedBlack", size: 21.0)]
        
        //tab bar clour setting
        UITabBar.appearance().tintColor = navigationColour
//        UITabBar.appearance().barTintColor = UIColor.whiteColor();
        
        
        // notification 

        application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: UIUserNotificationType.Sound|UIUserNotificationType.Alert|UIUserNotificationType.Badge, categories: nil))
        
        return true
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {

        println("hello, i received the notificaiton_______________")
        let rootViewController = self.window!.rootViewController as! TabbarController
        rootViewController.selectedIndex = 1;
        
    }
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

