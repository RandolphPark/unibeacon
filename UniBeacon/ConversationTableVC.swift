//
//  ConversationTableVC.swift
//  UniBeacon
//
//  Created by Randolph Park on 11/03/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//


import UIKit

class ConversationTableVC: UIViewController, UITableViewDelegate, UIAlertViewDelegate {

    var eventId = ""
    var messages = [PFObject]()
    @IBOutlet weak var spinningWheel: UIActivityIndicatorView!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        self.spinningWheel.hidden = false
        
        fetchDataOnView()
        
        self.spinningWheel.hidden = true
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Table View Delegte Function
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.messages.count
    }
    func configureTableView() {
        //println("i am configureing the table view ")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 120.0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let eachMessage:PFObject = self.messages[indexPath.row]
        let messageCotent = eachMessage["message"] as? String
        
        let localDisplayFormatString = "dd/MM HH:mm"
        let localDisplayFormatter = NSDateFormatter()
        let posIx = NSLocale(localeIdentifier: "en_AU")
        localDisplayFormatter.locale = posIx
        localDisplayFormatter.timeZone = NSTimeZone(name: "Australia/Brisbane")
        localDisplayFormatter.dateFormat = localDisplayFormatString;
        let messageCreatedTimeString = localDisplayFormatter.stringFromDate(eachMessage.createdAt)
        
        let authorName = eachMessage["authorName"] as? String
        let urlString = eachMessage["profileImageUrl"] as? String
        
        if authorName == PFUser.currentUser().username {
            let cellIdentifier = "MessageCellMe"
            let cell: MessageFromMeCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! MessageFromMeCell
            cell.messages.text = messageCotent
            cell.authorName.text = "Me"
            cell.postedTime.text = messageCreatedTimeString
            
            if urlString != "" && urlString != nil {
                let url = NSURL(string: urlString!)
                let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
                let image:UIImage = UIImage(data: data!)!
                
                
                cell.iconImage.layer.cornerRadius = 22.5
                cell.iconImage.layer.masksToBounds = true
                cell.iconImage.layer.borderWidth = 0.5
                cell.iconImage.image = image

            }

            return cell
            
        } else {
            
            let cellIdentifier = "MessageCellOthers"
            let cell: MessageFromOthers = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)as! MessageFromOthers
            cell.messages.text = messageCotent
            cell.authorName.text = authorName
            cell.postedTime.text = messageCreatedTimeString
            
            if urlString != "" && urlString != nil {
                let url = NSURL(string: urlString!)
                let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
                let image:UIImage = UIImage(data: data!)!
                
                
                cell.iconImage.layer.cornerRadius = 22.5
                cell.iconImage.layer.masksToBounds = true
                cell.iconImage.layer.borderWidth = 0.5
                cell.iconImage.image = image
                
            }
            
            return cell
        }

    }
    
    //    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    //        println(nameArray[indexPath.row])
    //    }
    
    
    //MARK: Supportive function
    func fetchDataOnView() -> Void {
        println("try to refetch the data for MESSAGE, event id is  \(self.eventId)")
        var query = PFQuery(className: "Message");
        query.orderByDescending("createdAt")
        query.whereKey("eventId", equalTo: self.eventId)
        query.findObjectsInBackgroundWithBlock { (objects: [AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                //println(objects)
                println("get \(objects.count) result")
                if let objects = objects as? [PFObject] {
                    self.messages = objects
                    self.tableView.reloadData()
                }
            } else {
                // Log details of the failure
                println("Error: \(error) \(error.userInfo!)")
            }
        }
    }
    
    
    
    @IBAction func popUpToCompose(sender: AnyObject) {
        
        let alert:UIAlertView = UIAlertView(title: "Announcement", message: "Write an announcement", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Send")
        alert.alertViewStyle = UIAlertViewStyle.PlainTextInput
        alert.show()
        //[alert release];
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        
        if buttonIndex == 0 {
            println("this is clancel")
        } else if buttonIndex == 1 {
            println("this is send")
            if let message = alertView.textFieldAtIndex(0)?.text {
                println(message)
                if message != "" {
                    self.composeMessage(message)
                }
            }
        }
    }
    
    func composeMessage( message: String) -> Void {
        println(" try to compose message to server")
        
        if message != "" {
            var newMessage = PFObject(className:"Message")
            newMessage["author"] = PFUser.currentUser()
            newMessage["authorName"] = PFUser.currentUser().username
            newMessage["eventId"] = self.eventId
            newMessage["message"] = message
            
            if let imageData:PFFile = PFUser.currentUser()["imageFile"] as? PFFile {
                newMessage["profileImageUrl"] = imageData.url
            }
            
            
            newMessage.save()
            fetchDataOnView()
        }
        
    }
}

