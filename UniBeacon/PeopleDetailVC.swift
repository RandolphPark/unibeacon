//
//  PeopleDetailVC.swift
//  UniBeacon
//
//  Created by Randolph Park on 14/03/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//

import UIKit

class PeopleDetailVC: UIViewController {
    
    var userName = ""

    @IBOutlet weak var peopleIcon: UIImageView!
    
    @IBOutlet weak var peopleName: UILabel!
    
    @IBOutlet weak var emailLink: UITextView!
    
    override func viewWillAppear(animated: Bool) {
        println(self.userName)
        fetchDataOnView()
    }
    
    func fetchDataOnView() -> Void {
        var query = PFQuery(className: "_User");
        query.whereKey("username", equalTo:self.userName)
        query.findObjectsInBackgroundWithBlock { (objects: [AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                //println(objects)
                println("get \(objects.count) result")
                if let objects = objects as? [PFObject] {
                    let user:PFObject = objects[0]
                    self.peopleName.text = user["username"] as? String
                    self.emailLink.text = user["email"] as? String
                    
                    if let imageData:PFFile = user["imageFile"] as? PFFile {
                        let url = NSURL(string: imageData.url)
                        let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
                        
                        
                        
                        // make it circle in display
                        self.peopleIcon.layer.cornerRadius = self.peopleIcon.frame.size.height/2
                        self.peopleIcon.layer.borderWidth = 0.5
                        self.peopleIcon.layer.masksToBounds = true
                        self.peopleIcon.image = UIImage(data: data!)
                        
                    }

                }
            } else {
                // Log details of the failure
                println("Error: \(error) \(error.userInfo!)")
            }
        }
    }

}