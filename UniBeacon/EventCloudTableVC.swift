//
//  EventCloudTableVC.swift
//  UniBeacon
//
//  Created by Randolph Park on 5/03/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//


import UIKit

class EventCloudTableVC: UITableViewController {
    
    //    let eventList = [ DashboardCellModel(name: "Group Meeting For WWACP", location: "Y406", startTime: "10:20", endTime: "12:20"),
    //        DashboardCellModel(name: "Lunch Chat at y block 601  space would be good xD ....(11 words)", location: "Y601", startTime: "12:30", endTime: "14:20"),
    //        DashboardCellModel(name: "Gym Booking", location: "P201", startTime: "17:20", endTime: "18:20") ]
    
    var eventId = ""
    var fileList = [PFObject]()
    
    @IBOutlet weak var dashboradTable: UITableView!
    
    @IBOutlet weak var spinningWheel: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        println("this is event cloud table view \(eventId)")
        
        fetchDataOnView()
        
        self.refreshControl?.addTarget(self, action: "refreshByDrag:", forControlEvents: UIControlEvents.ValueChanged)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Table View Delegte Function
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
        //        return 2
    }
    override func tableView(tableView: UITableView,
        titleForHeaderInSection section: Int)
        -> String {
            //            if (section == 0) {
            //                return "ongoing List"
            //            } else {
            //                return "upcoming event"
            //            }
            return "File List"
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.fileList.count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cellIdentifier = "aEventCloudCell"
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! UITableViewCell
        
        
        let file = self.fileList[indexPath.row]
//        cell.textLabel?.text = "hello"
        cell.textLabel?.text = file["fileName"] as? String
//
//        cell.name.text = event.name
//        cell.time.text = event.startTime + " ~ " + event.endTime
//        cell.location.text = event.location
        
        return cell
        
    }
    
    //    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    //        println(nameArray[indexPath.row])
    //    }
    
    
    //MARK: Supportive function
    func fetchDataOnView() -> Void {
        println("try to refetch the data")
        var query = PFQuery(className: "File");
        query.whereKey("event", equalTo: PFObject(withoutDataWithClassName: "EventDetail", objectId: self.eventId))
        query.findObjectsInBackgroundWithBlock { (objects: [AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                //println(objects)
                println("get \(objects.count) result")
                if let objects = objects as? [PFObject] {
                    self.fileList = objects
                    self.tableView.reloadData()
                }
            } else {
                // Log details of the failure
                println("Error: \(error) \(error.userInfo!)")
            }
        }
    }
    
    func refreshByDrag(sender:AnyObject)
    {
        // Updating your data here...
        
        fetchDataOnView()
        self.refreshControl?.endRefreshing()
        println("finished refresh\(MyFunction.currentTimeInLocal())")
    }
    
    //MARK: Segue related function 
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "eventCloudCellPressed") {
            var indexPath:NSIndexPath = self.tableView.indexPathForCell(sender as! UITableViewCell)!
            var file:PFObject = self.fileList[indexPath.row]
            
            let singleFileDetail: FileDetailVC = segue.destinationViewController as! FileDetailVC
            
            var temp = file["fileName"] as! String
            singleFileDetail.aFileName = temp
            println("Parse File name is \(temp)")
            singleFileDetail.aFileId = file.objectId as String
            singleFileDetail.aFileURL = file["fileUrl"] as! String
            singleFileDetail.aFileDescription = file["fileDescription"] as! String
            singleFileDetail.aAuthor = file["authorName"] as! String
            singleFileDetail.aFileIconUrl = file["iconUrl"] as! String
            
            
        } else if (segue.identifier == "addFilePressed") {
            var addFileVC: AddFileCV = segue.destinationViewController as! AddFileCV;
            addFileVC.eventId = self.eventId
        }
    }

    //MARK: Segue related method
    @IBAction func unwindToFileList(segue: UIStoryboardSegue) {
        println("jump back .........\(self.eventId)")
//        var query = PFQuery(className: "File");
//        query.whereKey("event", equalTo: PFObject(withoutDataWithClassName: "EventDetail", objectId: self.eventId))
//        query.findObjects()
//        query.findObjectsInBackgroundWithBlock { (objects: [AnyObject]!, error: NSError!) -> Void in
//            if error == nil {
//                //println(objects)
//                println("get \(objects.count) result")
//                if let objects = objects as? [PFObject] {
//                    self.fileList = objects
//                    println("get new data from the parse")
//                    println(self.fileList)
//                    self.tableView.reloadData()
//                }
//            } else {
//                // Log details of the failure
//                println("Error: \(error) \(error.userInfo!)")
//            }
//        }

    }

    
}

