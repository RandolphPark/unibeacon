//
//  PeopleCustomCell.swift
//  UniBeacon
//
//  Created by Randolph Park on 13/03/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//


import UIKit

class PeopleCustomCell: UITableViewCell {
    

    @IBOutlet weak var peopleName: UILabel!
    
    @IBOutlet weak var peopleIcon: UIImageView!
    
    @IBOutlet weak var peopleStatusView: UIView!
    
}