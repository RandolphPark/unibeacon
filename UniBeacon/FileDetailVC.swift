//
//  FileDetailVC.swift
//  UniBeacon
//
//  Created by Randolph Park on 6/03/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//

import UIKit

class FileDetailVC: UIViewController {
    
    var aFileURL = ""
    var aFileName = "xxx.pdf"
    var aAuthor = "// kim lee"
    var aFileDescription = "test only"
    var aFileId = ""
    var aFileIconUrl = ""
    
    
    @IBOutlet weak var deleteBTN: UIButton!
    @IBOutlet weak var fileName: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var fileDescription: UILabel!
    @IBOutlet weak var fileIconImage: UIImageView!
    
    @IBAction func openURL(sender: AnyObject) {
        println("open url pressed")
        if self.aFileURL != "" {
            UIApplication.sharedApplication().openURL(NSURL(string: self.aFileURL)!);
        }
        
    }
    @IBAction func deleteCurrentFile(sender: AnyObject) {
        println("file ID is \(self.aFileId)")
        
        var file = PFObject(withoutDataWithClassName: "File", objectId: self.aFileId);
        file.delete()
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        println("view did appear in file detail view")
        self.fileName.text = "File Name : " + self.aFileName;
        self.author.text = "Uploaded By : " +  self.aAuthor
        if self.aFileDescription == "" {
            self.fileDescription.text = "File Description : Not Provided"
        } else {
            self.fileDescription.text = "File Description : " + self.aFileDescription
        }
        
        
        println(self.aAuthor)
        println(PFUser.currentUser().username)
        if self.aAuthor != PFUser.currentUser().username {
            self.deleteBTN.hidden = true
        } else {
            self.deleteBTN.hidden = false
        }
        
        let url = NSURL(string: self.aFileIconUrl)
        if let data = NSData(contentsOfURL: url!) {
            self.fileIconImage.image = UIImage(data: data)
        } else {
            // no image file
        }
    }
}