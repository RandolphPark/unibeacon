//
//  EventCreationRoomPickerCell.swift
//  UniBeacon
//
//  Created by Randolph Park on 24/03/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//

import UIKit

class EventCreationRoomPickerCell: UITableViewCell,UIPickerViewDelegate {
    
    @IBOutlet weak var roomPicker: UIPickerView!
    
}