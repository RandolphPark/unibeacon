//
//  MessageFromOthers.swift
//  UniBeacon
//
//  Created by Randolph Park on 13/03/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//


import UIKit

class MessageFromOthers: UITableViewCell {
    
    @IBOutlet weak var authorName: UILabel!
    @IBOutlet weak var messages: UILabel!
    @IBOutlet weak var postedTime: UILabel!
    @IBOutlet weak var iconImage: UIImageView!

}