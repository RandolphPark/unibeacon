//
//  AddFileCV.swift
//  UniBeacon
//
//  Created by Randolph Park on 6/03/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//

import UIKit

class AddFileCV: UIViewController, UITextFieldDelegate {


    var eventId = "";
    var fileNameString = "";
    var fileIconUrl = "";
    var fileUrl = "";
    
    
    
    @IBOutlet weak var filedDescription: UITextField!
    @IBOutlet weak var fileUrlLabel: UILabel!
    @IBOutlet weak var fileName: UILabel!
    @IBOutlet weak var fileIconImage: UIImageView!
    


    
    override func viewDidLoad() {
        super.viewDidLoad()
        println("DashboardVC did load")
        self.filedDescription.delegate = self
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
       
    }
    
    
    @IBAction func go2DropBox(sender: AnyObject) {

        DBChooser.defaultChooser().openChooserForLinkType(DBChooserLinkTypePreview, fromViewController: self, completion:{(results) in
            
            if (results != nil) {
                var temp = NSArray(array: results);
                let fileInfo:DBChooserResult = temp[0] as! DBChooserResult;
                
                self.fileUrl = fileInfo.link.absoluteString!
                self.fileNameString = fileInfo.name
                self.fileIconUrl = fileInfo.iconURL.absoluteString!
                
                self.fileName.text = self.fileNameString
                self.fileUrlLabel.text = self.fileUrl
                
                let url = NSURL(string: self.fileIconUrl)
                if let data = NSData(contentsOfURL: url!) {
                    self.fileIconImage.image = UIImage(data: data)
                } else {
                    // no image file
                }
                
                
            } else {
                println("user canceled the action")
            }
            
        })

    }

    @IBAction func saveFileToCloud(sender: AnyObject) {
        
        if self.fileUrl == "" {
            let alert = UIAlertController(title: "Error", message: "Please get file from dropbox ", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            println("finished save")
            var newFile = PFObject(className:"File")
            newFile["fileUrl"] = self.fileUrl
            newFile["iconUrl"] = self.fileIconUrl
            newFile["fileName"] = self.fileNameString
            newFile["author"] = PFUser.currentUser()
            newFile["authorName"] = PFUser.currentUser().username
            newFile["fileDescription"] = self.filedDescription.text
            newFile["event"] = PFObject(withoutDataWithClassName: "EventDetail", objectId: self.eventId)
            newFile["eventId"] = self.eventId
            newFile.save()
            self.performSegueWithIdentifier("unwindToFileList", sender: self)
        }

    }
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        println("using the segue");
//    }
    
    //MARK: TextField Delegate Function
//    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
//        
//        self.view.endEditing(true)
//    }
//    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        println("entered return")
        filedDescription.resignFirstResponder()
        return true
    }
    

}