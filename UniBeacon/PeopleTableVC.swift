//
//  EventCloudTableVC.swift
//  UniBeacon
//
//  Created by Randolph Park on 5/03/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//


import UIKit

class PeopleTableVC: UITableViewController {
    
    
    var eventId = ""
    var peopleListIn = [PFObject]()
    var peopleListOut = [PFObject]()
    
    @IBOutlet weak var dashboradTable: UITableView!
    
    @IBOutlet weak var spinningWheel: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        println("this is event cloud table view \(eventId)")
        
        fetchDataOnView()
        
        self.refreshControl?.addTarget(self, action: "refreshByDrag:", forControlEvents: UIControlEvents.ValueChanged)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Table View Delegte Function
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
        //        return 2
    }
    override func tableView(tableView: UITableView,
        titleForHeaderInSection section: Int)
        -> String {
            if (section == 0) {
                return "People In"
            } else {
                return "People Out"
            }
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (section == 0) {
            return self.peopleListIn.count
        } else {
            return self.peopleListOut.count
        }
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cellIdentifier = "peopleCell"
            let cell: PeopleCustomCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PeopleCustomCell
            let people = self.peopleListIn[indexPath.row]
            cell.peopleName.text = people["userName"] as? String
            cell.peopleStatusView.backgroundColor = UIColor(red:122/255.0, green:230/255.0, blue:0/255.0, alpha:1.0)
            
            let urlString = people["profileImageUrl"] as? String
            if urlString != "" && urlString != nil {
                let url = NSURL(string: urlString!)
                let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
                let image:UIImage = UIImage(data: data!)!
                
                
                cell.peopleIcon.layer.cornerRadius = 22.5
                cell.peopleIcon.layer.masksToBounds = true
                cell.peopleIcon.layer.borderWidth = 0.5
                cell.peopleIcon.image = image
                
            }
            
            return cell
            
        } else {
            let cellIdentifier = "peopleCell"
            let cell: PeopleCustomCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PeopleCustomCell
            let people = self.peopleListOut[indexPath.row]
            cell.peopleName.text = people["userName"] as? String
            cell.peopleStatusView.backgroundColor = UIColor(red:202/255.0, green:1/255.0, blue:0/255.0, alpha:1.0)
            
            let urlString = people["profileImageUrl"] as? String
            if urlString != "" && urlString != nil {
                let url = NSURL(string: urlString!)
                let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
                let image:UIImage = UIImage(data: data!)!
                
                
                cell.peopleIcon.layer.cornerRadius = 22.5
                cell.peopleIcon.layer.masksToBounds = true
                cell.peopleIcon.layer.borderWidth = 0.5
                cell.peopleIcon.image = image
                
            }
            return cell
        }
        
    }
    
    //    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    //        println(nameArray[indexPath.row])
    //    }
    
    
    //MARK: Supportive function
    func fetchDataOnView() -> Void {
        
        println("try to refetch the data")
        self.peopleListOut = [PFObject]()
        self.peopleListIn = [PFObject]()
        var query = PFQuery(className: "Status");
        query.whereKey("eventDetail", equalTo: PFObject(withoutDataWithClassName: "EventDetail", objectId: self.eventId))
        query.findObjectsInBackgroundWithBlock { (objects: [AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                //println(objects)
                println("get \(objects.count) result")
                if let objects = objects as? [PFObject] {
                    for object in objects {
                        if object["statusIn"] as! Bool == true {
                            self.peopleListIn.append(object)
                        } else {
                            self.peopleListOut.append(object)
                        }
                    }
                    self.tableView.reloadData()
                }
            } else {
                // Log details of the failure
                println("Error: \(error) \(error.userInfo!)")
            }
        }
    }
    
    func refreshByDrag(sender:AnyObject)
    {
        // Updating your data here...
        
        fetchDataOnView()
        self.refreshControl?.endRefreshing()
        println("finished refresh\(MyFunction.currentTimeInLocal())")
    }
    
    //MARK: Segue related function
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "peopleDetailPressed") {
            var indexPath:NSIndexPath = self.tableView.indexPathForCell(sender as! UITableViewCell)!
            println("index path is \(indexPath.section) - \(indexPath.row)")
            
            if indexPath.section == 0 {
                let user: PFObject = self.peopleListIn[indexPath.row] as PFObject
                let userName = user["userName"]as? String
                let peopleDetailVC:PeopleDetailVC = segue.destinationViewController as! PeopleDetailVC
                peopleDetailVC.userName = userName!
                
            } else {
                let user: PFObject = self.peopleListOut[indexPath.row] as PFObject
                let userName = user["userName"]as? String
                let peopleDetailVC:PeopleDetailVC = segue.destinationViewController as! PeopleDetailVC
                peopleDetailVC.userName = userName!
            }

        } 
    }
    
}

