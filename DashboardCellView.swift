//
//  DashboardCellView.swift
//  UniBeacon
//
//  Created by Randolph Park on 25/02/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//

import UIKit

class DashboardCellView: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var location: UILabel!
}