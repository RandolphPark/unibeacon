//
//  DashboardCellModel.swift
//  UniBeacon
//
//  Created by Randolph Park on 25/02/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//

import UIKit

class DashboardCellModel: NSObject {
    var name: String
    var location: String
    var startTime: String
    var endTime: String
    
    init(name: String, location: String, startTime: String, endTime: String) {
        self.name = name
        self.location = location
        self.startTime = startTime
        self.endTime = endTime
        super.init()
    }
}