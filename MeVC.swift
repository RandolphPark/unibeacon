//
//  MeVC.swift
//  UniBeacon
//
//  Created by Randolph Park on 22/02/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//

import UIKit
import ParseUI

class MeVC: UIViewController,PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    

    var imagePicker = UIImagePickerController();
    
    @IBOutlet weak var myEmail: UILabel!
    @IBOutlet weak var myName: UILabel!
    
    @IBAction func logOut(sender: AnyObject) {
        
        PFUser.logOut()
        
        var image : UIImage = UIImage(named:"avartar@30%.png")!
        self.profileImage.setImage(image, forState: .Normal)
        
        var logInVC:PFLogInViewController = PFLogInViewController()
        logInVC.fields = PFLogInFields.UsernameAndPassword | PFLogInFields.LogInButton | PFLogInFields.SignUpButton | PFLogInFields.PasswordForgotten
        logInVC.logInView.logo = UIImageView(image: UIImage(named: "logo.png"))
        logInVC.delegate = self
        
        var signUpVC:PFSignUpViewController = PFSignUpViewController()
        signUpVC.signUpView.logo = UIImageView(image: UIImage(named: "logo.png"))
        signUpVC.delegate = self
        
        logInVC.signUpController = signUpVC
        
        self.presentViewController(logInVC, animated: false, completion: nil)

    }
    
    @IBOutlet weak var profileImage: UIButton!
    @IBAction func updateProfileImage(sender: AnyObject) {
        
        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = true
        self.imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(self.imagePicker, animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (PFUser.currentUser() != nil) {
            self.myEmail.text = PFUser.currentUser().email
            self.myName.text = PFUser.currentUser().username
            // update profile image
            if let imageData:PFFile = PFUser.currentUser()["imageFile"] as? PFFile {
                let url = NSURL(string: imageData.url)
                let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
                
                
                
                // make it circle in display
                self.profileImage.imageView!.layer.cornerRadius = self.profileImage.imageView!.frame.size.height/2
                self.profileImage.imageView!.layer.borderWidth = 0.5
                self.profileImage.layer.masksToBounds = true
                self.profileImage.setImage(UIImage(data: data!), forState: .Normal)
                
            }
        }
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Image Picker delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        let selectedImage:UIImage = info[UIImagePickerControllerEditedImage] as! UIImage

        // resize image
        let smallImageSize : CGSize = CGSizeMake(60, 60)
        UIGraphicsBeginImageContext(smallImageSize);
        selectedImage.drawInRect(CGRectMake(0, 0, smallImageSize.width, smallImageSize.height))
        let smallerImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        println(" smaller image size is \(smallerImage.size)")
        // assign image
        self.profileImage.setImage(smallerImage, forState: .Normal)
        
        // make it circle in display
        self.profileImage.imageView!.layer.cornerRadius = self.profileImage.imageView!.frame.size.height/2
        self.profileImage.imageView!.layer.borderWidth = 0.5
        self.profileImage.layer.masksToBounds = true
        
        let imageData = UIImagePNGRepresentation(smallerImage)
        let imageFile = PFFile(name:"image.png", data:imageData)
        
        var user = PFUser.currentUser()
        user["imageFile"] = imageFile
        user.saveInBackgroundWithBlock {
            (success: Bool, error: NSError!) -> Void in
            if (success) {
                // The object has been saved.
                println("image is saved")
            } else {
                let alert = UIAlertController(title: "Image Update Err", message: "Err occured when uploading the image. Please try agian", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                println("image is not saved")
                // There was a problem, check error.description
            }
        }
        
//        imageFile.saveInBackgroundWithBlock {
//            (succeeded: Bool!, error: NSError!) -> Void in
//            println("image profile saved")
//            // Handle success or failure here ...
//        },progressBlock: {
//            (percentDone: Int) -> Void in
//            println("current progress is \(percentDone)")
//            // Update your progress spinner here. percentDone will be between 0 and 100.
//        }
        
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK: - PF Login ViewControllerDelegate
    // Sent to the delegate to determine whether the log in request should be submitted to the server.
    func logInViewController(logInController: PFLogInViewController!, shouldBeginLogInWithUsername username: String!, password: String!) -> Bool {
        
        if ((username) != nil && password != nil && count(username) != 0 && count(password) != 0) {
            return true;
        }
        
        var alert = UIAlertView(title: "Missing Informaiton", message: "Make sure you fill out all of the information", delegate: nil, cancelButtonTitle: "OK")
        
        alert.show()
        return false
    }
    
    
    func logInViewController(logInController: PFLogInViewController!, didLogInUser user: PFUser!) {
        var userName = user.username
        println("User name is \(userName)")
        self.myEmail.text = user.email
        self.myName.text = userName
        if let imageData:PFFile = PFUser.currentUser()["imageFile"] as? PFFile {
            let url = NSURL(string: imageData.url)
            let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
            
            
            
            // make it circle in display
            self.profileImage.imageView!.layer.cornerRadius = self.profileImage.imageView!.frame.size.height/2
            self.profileImage.imageView!.layer.borderWidth = 0.5
            self.profileImage.layer.masksToBounds = true
            self.profileImage.setImage(UIImage(data: data!), forState: .Normal)
            
        }

        self.dismissViewControllerAnimated(true, completion: nil)

        //        [self dismissViewControllerAnimated:YES completion:NULL];
    }
    
    func logInViewController(logInController: PFLogInViewController!, didFailToLogInWithError error: NSError!) {
        var alert = UIAlertView(title: "Fail to login", message: "Make sure you fill in correct username and password!", delegate: nil, cancelButtonTitle: "OK")
        
        alert.show()
    }
    
    // MARK: - PF SignUp ViewControllerDelegate
    
    func signUpViewController(signUpController: PFSignUpViewController!, shouldBeginSignUp info: [NSObject : AnyObject]!) -> Bool {
        
        println("submitted infor is \(info)")
        var informaitonCompelete = true
        
        let infoList = info as! Dictionary<String, String>
        let email = infoList["email"]
        let username = infoList["username"]
        let password = infoList["password"]
        
        if(email == nil || username == nil || password == nil ) {
            informaitonCompelete = false
            
            var alert = UIAlertView(title: "Missing Information", message: "Make sure you fill out all of the information.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
        return informaitonCompelete
    }
    
    func signUpViewController(signUpController: PFSignUpViewController!, didSignUpUser user: PFUser!) {
        println("signed up successfully")
        self.myEmail.text = user.email
        self.myName.text = user.username
        if let imageData:PFFile = PFUser.currentUser()["imageFile"] as? PFFile {
            let url = NSURL(string: imageData.url)
            let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
            
            
            
            // make it circle in display
            self.profileImage.imageView!.layer.cornerRadius = self.profileImage.imageView!.frame.size.height/2
            self.profileImage.imageView!.layer.borderWidth = 0.5
            self.profileImage.layer.masksToBounds = true
            self.profileImage.setImage(UIImage(data: data!), forState: .Normal)
            
        }

        self.dismissViewControllerAnimated(true, completion: nil)

    }
    
    func signUpViewController(signUpController: PFSignUpViewController!, didFailToSignUpWithError error: NSError!) {
        println("fail to singup ")
        var alert = UIAlertView(title: "Fail to sign up", message: "Please try again", delegate: nil, cancelButtonTitle: "OK")
        alert.show()
    }
    
    func signUpViewControllerDidCancelSignUp(signUpController: PFSignUpViewController!) {
        println("user dismissed the singupview controller")
    }
    

}

