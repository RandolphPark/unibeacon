//
//  TabbarController.swift
//  UniBeacon
//
//  Created by Randolph Park on 22/02/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//

import UIKit
import ParseUI
import CoreLocation
import Foundation
import SystemConfiguration


struct MyKeys {
    static var MINOR = "minor"
    static var MAJOR = "major"
    static var UUID = "UUID"
}

struct MyFunction {
    static func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0)).takeRetainedValue()
        }
        
        var flags: SCNetworkReachabilityFlags = 0
        if SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) == 0 {
            return false
        }
        
        let isReachable = (flags & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection) ? true : false
    }
    
    static func currentTimeInUtc() -> String{
        let utcTimeZone = NSTimeZone(abbreviation: "UTC")
        let formatter = NSDateFormatter()
        let posIx = NSLocale(localeIdentifier: "en_AU")
        formatter.locale = posIx
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        formatter.timeZone = utcTimeZone
        let now = NSDate()
        let utcTimeString = formatter.stringFromDate(now)
        return utcTimeString
    }
    
    static func currentTimeInLocal() -> String {
        let formatter = NSDateFormatter()
        let posIx = NSLocale(localeIdentifier: "en_AU")
        formatter.locale = posIx
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        formatter.timeZone  = NSTimeZone(name: "Australia/Brisbane")
        let now = NSDate()
        let currentTimeString = formatter.stringFromDate(now)
        return currentTimeString
        
    }
}
struct MyBeacon {
    static var major :NSNumber = 0
    static var minor :NSNumber = 0
//    static var enteredHistory : NSMutableDictionary = NSMutableDictionary()
//    static var exitedHistory : NSMutableDictionary = NSMutableDictionary()
    
//    static func enteredAndStaying() -> Bool{
//        var isStyaing = false;
//        
//        let now = NSDate()
//        let regionKey = "\(major)&\(minor)"
////        println(exitedHistory.valueForKey(regionKey))
//        if let lastEntered:NSDate = enteredHistory.valueForKey(regionKey) as? NSDate {
//            if now.timeIntervalSinceDate(lastEntered) < 60 {
//                isStyaing = true
//            }
//            println("current time- \(now), replaced \(lastEntered) / with region key \(regionKey) IN")
//        }
//        
//        enteredHistory.setValue(now, forKey: regionKey);
//        return isStyaing
//    }
//    
//    static func exitedButStaying(majorValue:String, minorValue:String) -> Bool{
//        var isStyaing = false;
//        
//        let now = NSDate()
//        let regionKey = "\(majorValue)&\(minorValue)"
//        
//        println(exitedHistory.valueForKey(regionKey))
//        if let lastEntered:NSDate = exitedHistory.valueForKey(regionKey) as? NSDate {
//            if now.timeIntervalSinceDate(lastEntered) < 60 {
//                isStyaing = true
//            }
//            println("current time- \(now), replaced \(lastEntered) / with region key \(regionKey) OUT")
//
//        }
//        
//        exitedHistory.setValue(now, forKey: regionKey);
//        
//        return isStyaing
//    }

}

struct MyEvent {
    static var currentEvent : String!
}

class TabbarController: UITabBarController, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate, UITextFieldDelegate, CLLocationManagerDelegate  {
    
    let locationManager = CLLocationManager()
    var iBeacons = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        println("TabbarVC did load")
        
        if(locationManager.respondsToSelector("requestAlwaysAuthorization")) {
            locationManager.requestAlwaysAuthorization()
        }
        locationManager.delegate = self
        locationManager.pausesLocationUpdatesAutomatically = false
        
//        var helloWorldTimer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: Selector("testConstantUpdateEnter"), userInfo: nil, repeats: true)
    
//        var helloWorldTimer2 = NSTimer.scheduledTimerWithTimeInterval(5.5, target: self, selector: Selector("testConstantUpdateExit"), userInfo: nil, repeats: true)
  
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        println("view did appear ")
        
        // Logic of login and display loginview
        println("current user is \(PFUser.currentUser())" )
        if (PFUser.currentUser() == nil) {
            var logInVC:PFLogInViewController = PFLogInViewController()
            logInVC.fields = PFLogInFields.UsernameAndPassword | PFLogInFields.LogInButton | PFLogInFields.SignUpButton | PFLogInFields.PasswordForgotten
            logInVC.logInView.logo = UIImageView(image: UIImage(named: "logo.png"))
            logInVC.delegate = self
            
            var signUpVC:PFSignUpViewController = PFSignUpViewController()
            signUpVC.signUpView.logo = UIImageView(image: UIImage(named: "logo.png"))
            signUpVC.delegate = self
            
            logInVC.signUpController = signUpVC
            
            self.presentViewController(logInVC, animated: false, completion: nil)
        } else {
            // start to monitoring the ibeancon when there is already a user
            updateLocalDBAndStartMornitoring()
        }
        //        PFUser.logOut()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: Beacon Related Function
    func updateLocalDBAndStartMornitoring() -> Void {
        
        self.updateBeaconDataIntoLocalDataStore()
        self.startMornitorinBeacon()

    }

    
    func updateBeaconDataIntoLocalDataStore() ->Void {
        //println("start to update local DB")
        
        if( MyFunction.isConnectedToNetwork() == true ) {
            var query = PFQuery(className:"iBeacon")
            if let objects = query.findObjects() {
                // update data into local storeage in BACKGROUND
                //PFObject.pinAllInBackground(objects, block: nil)
                self.iBeacons = objects
                //            PFObject.pinAlli(objects)
                PFObject.pinAllInBackground(objects, block: nil)
                println("Internet CONNECTED and finshed all update to local DB")
            }
        } else {
            var query = PFQuery(className:"iBeacon")
            query.fromLocalDatastore()
            if let objects = query.findObjects() {
                self.iBeacons = objects
                println("No Internet . use data from original DB")
            }

        }
        
    }
    
    func startMornitorinBeacon() -> Void {
        println("this is all ibeacons\(self.iBeacons)")
        if let myIbeacons = self.iBeacons as? [PFObject] {
            // start to mornitor
            for iBeacon in myIbeacons {
                let uuidString = iBeacon.objectForKey("UUID")! as! String
                let uuid:NSUUID = NSUUID(UUIDString: uuidString)!
                
                let majorValue = iBeacon.objectForKey(MyKeys.MAJOR)as! NSNumber
                let major:CLBeaconMajorValue = UInt16(majorValue.integerValue)
                let minorValue = iBeacon.objectForKey(MyKeys.MINOR) as! NSNumber
                let minor:CLBeaconMinorValue = UInt16(minorValue.integerValue)
                
                let identifier = "\(major)-\(minor)"
                println("identifier i put \(major)-\(minor)")
                
                let beaconRegion:CLBeaconRegion = CLBeaconRegion(proximityUUID: uuid, major: major, minor: minor, identifier: identifier)
                self.locationManager.startMonitoringForRegion(beaconRegion)
            }
            
            // functions to rangging beacons
//
//            let uuidString:String = "B9407F30-F5F8-466E-AFF9-25556B57FE6D"
//            let region = CLBeaconRegion(proximityUUID: NSUUID(UUIDString:uuidString), identifier: "estimotes")
//            self.locationManager.startRangingBeaconsInRegion(region)
            //println("finished monitoring all beacons")
            
        }
    }
    
//    // MARK: Beacon Delegate Function - rangging beacons
//    func locationManager(manager: CLLocationManager!,
//        didRangeBeacons beacons: [AnyObject]!,
//        inRegion region: CLBeaconRegion!) {
//            
//            var message:String = ""
//            if(beacons.count > 0) {
//                let nearestBeacon:CLBeacon = beacons[0] as CLBeacon
//                
//                if (MyBeacon.previousBeacon == nil) {
//                    MyBeacon.previousBeacon = nearestBeacon
//                    //println("thisi is first time to put clbeacon into previous veacon")
//                } else if (MyBeacon.previousBeacon.major == nearestBeacon.major && MyBeacon.previousBeacon.minor == nearestBeacon.minor && MyBeacon.previousBeacon.proximityUUID == nearestBeacon.proximityUUID) {
//                    //todo: beacon not changed
//                    //println("current baecon is same as previous beacon so not change ")
//                } else {
//                     //println("current baecon is differnt from previous beacon  ")
//                    MyBeacon.previousBeacon = nearestBeacon
//                    //todo: behavior when beacon changed
//                }
//                //mark:println("beacon major:\(nearestBeacon.major)")
//                switch nearestBeacon.proximity {
//                case CLProximity.Far:
//                    message = "You are far away from the beacon"
//                case CLProximity.Near:
//                    message = "You are near the beacon"
//                case CLProximity.Immediate:
//                    message = "You are in the immediate proximity of the beacon"
//                case CLProximity.Unknown:
//                    return
//                }
//                
//            } else {
//                message = "No beacons are nearby"
//            }
//            
//            //NSLog("%@", message)
//            //sendLocalNotificationWithMessage(message)
//    }
    
    func locationManager(manager: CLLocationManager!,
        didEnterRegion region: CLRegion!) {
            NSLog("You entered the region")
            let identifier = region.identifier
            println("i got identifer\(identifier)")
            var identifierArr = split(identifier) {$0 == "-"}
            var major: String = identifierArr[0]
            var minor: String! = identifierArr.count > 1 ? identifierArr[1] : nil
            var isSameAsBeacon = false
            
            let previousMajor = "\(MyBeacon.major)"
            let previousnewMinor = "\(MyBeacon.minor)"
            
            if (major == previousMajor && minor == previousMajor) {
                isSameAsBeacon = true
                println("it is same beacon")
            }
            
            MyBeacon.major = major.toInt()!
            MyBeacon.minor = minor.toInt()!
            let userId:String = PFUser.currentUser().objectId
            let userName:String = PFUser.currentUser().username
            
            var imageUrl = ""
            if let imageData:PFFile = PFUser.currentUser()["imageFile"] as? PFFile {
                imageUrl = imageData.url
            }
            
            PFCloud.callFunctionInBackground("updateIn", withParameters: ["nowTime" : MyFunction.currentTimeInUtc(), "major":major.toInt()!, "minor":minor.toInt()!, "userId":userId, "userName":userName, "profileImageUrl":imageUrl]) {
                (response : AnyObject!, error : NSError!) -> Void in
                
                let tempString:String = response as! String
                if (tempString != "No Event") {
                    let alert = UIAlertController(title: "Notice", message: "You are connected with new event", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    if ( isSameAsBeacon != true) {
                        var localNotification:UILocalNotification = UILocalNotification()
                        localNotification.alertAction = "Notice"
                        localNotification.alertBody = "You entered into an event area"
                        localNotification.fireDate = NSDate(timeIntervalSinceNow: 1)
                        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                        
                        NSNotificationCenter.defaultCenter().postNotificationName("updateBeaconInformationToIn", object: nil)
                    }

                }
            }
    }

    func locationManager(manager: CLLocationManager!,
        didExitRegion region: CLRegion!) {
            NSLog("You exited the region")
            let identifier = region.identifier
            println("i got identifer\(identifier)")
            var identifierArr = split(identifier) {$0 == "-"}
            var major: String = identifierArr[0]
            var minor: String! = identifierArr.count > 1 ? identifierArr[1] : nil
            
            let previousMajor = "\(MyBeacon.major)"
            let previousnewMinor = "\(MyBeacon.minor)"
            
            if (major == previousMajor && minor == previousnewMinor) {
                MyBeacon.major = 0
                MyBeacon.minor = 0
                println("exit from previous ")
                
                NSNotificationCenter.defaultCenter().postNotificationName("updateBeaconInformationToOut", object: nil)
            }

            let userId:String = PFUser.currentUser().objectId
            let userName:String = PFUser.currentUser().username
            println("\(MyFunction.currentTimeInUtc()) - \(major)-\(minor)-\(userId)-\(userName)")
            var imageUrl = ""
            if let imageData:PFFile = PFUser.currentUser()["imageFile"] as? PFFile {
                imageUrl = imageData.url
            }
            
            PFCloud.callFunctionInBackground("updateOut", withParameters: ["nowTime" : MyFunction.currentTimeInUtc(), "major":major.toInt()!, "minor":minor.toInt()!, "userId":userId,"userName":userName, "profileImageUrl":imageUrl]) {
                (response : AnyObject!, error : NSError!) -> Void in
                
                let tempString:String = response as! String
                if (tempString != "No Event") {

//                    let alert = UIAlertController(title: "Notice", message: "You are disconnected with previous event", preferredStyle: UIAlertControllerStyle.Alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
//                    self.presentViewController(alert, animated: true, completion: nil)

                }
//                if error == nil {
//                    
//                }
            }
            
            
    }
    
    
    // MARK: - PF Login ViewControllerDelegate
    // Sent to the delegate to determine whether the log in request should be submitted to the server.
    func logInViewController(logInController: PFLogInViewController!, shouldBeginLogInWithUsername username: String!, password: String!) -> Bool {
        
        if ((username) != nil && password != nil && count(username) != 0 && count(password) != 0) {
            return true;
        }
        
        var alert = UIAlertView(title: "Missing Informaiton", message: "Make sure you fill out all of the information", delegate: nil, cancelButtonTitle: "OK")
        
        alert.show()
        return false
    }
    
    
    func logInViewController(logInController: PFLogInViewController!, didLogInUser user: PFUser!) {
        var userName = user.username
        println("User name is \(userName)")
        self.dismissViewControllerAnimated(true, completion: nil)
        updateLocalDBAndStartMornitoring()
        
//        [self dismissViewControllerAnimated:YES completion:NULL];
    }
    
    func logInViewController(logInController: PFLogInViewController!, didFailToLogInWithError error: NSError!) {
        var alert = UIAlertView(title: "Fail to login", message: "Make sure you fill in correct username and password!", delegate: nil, cancelButtonTitle: "OK")
        
        alert.show()
    }
    
    // MARK: - PF SignUp ViewControllerDelegate
    
    func signUpViewController(signUpController: PFSignUpViewController!, shouldBeginSignUp info: [NSObject : AnyObject]!) -> Bool {
        
        println("submitted infor is \(info)")
        var informaitonCompelete = true
        
        let infoList = info as! Dictionary<String, String>
        let email = infoList["email"]
        let username = infoList["username"]
        let password = infoList["password"]
        
        if(email == nil || username == nil || password == nil ) {
            informaitonCompelete = false
            
            var alert = UIAlertView(title: "Missing Information", message: "Make sure you fill out all of the information.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
        return informaitonCompelete
    }
    
    func signUpViewController(signUpController: PFSignUpViewController!, didSignUpUser user: PFUser!) {
        println("signed up successfully")
        self.dismissViewControllerAnimated(true, completion: nil)
        updateLocalDBAndStartMornitoring()
        
    }
    
    func signUpViewController(signUpController: PFSignUpViewController!, didFailToSignUpWithError error: NSError!) {
        println("fail to singup ")
        var alert = UIAlertView(title: "Fail to sign up", message: "Please try again", delegate: nil, cancelButtonTitle: "OK")
        alert.show()
    }
    
    func signUpViewControllerDidCancelSignUp(signUpController: PFSignUpViewController!) {
        println("user dismissed the singupview controller")
    }

    // MARK: Supportive function
    func sendLocalNotificationWithMessage(message: String!) {
        let notification:UILocalNotification = UILocalNotification()
        notification.alertBody = message
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }
//    
//    func testConstantUpdateEnter()-> Void {
//
//        let beaconRegion:CLBeaconRegion = CLBeaconRegion(proximityUUID:  NSUUID(UUIDString:"B9407F30-F5F8-466E-AFF9-25556B57FE6D"), major: 53476, minor: 39412, identifier: "53476-39412")
//        self.locationManager(locationManager, didEnterRegion: beaconRegion)
//
//    }
//    func testConstantUpdateExit()-> Void {
//        let beaconRegion:CLBeaconRegion = CLBeaconRegion(proximityUUID:  NSUUID(UUIDString:"B9407F30-F5F8-466E-AFF9-25556B57FE6D"), major: 53476, minor: 39412, identifier: "53476-39412")
//        self.locationManager(locationManager, didExitRegion: beaconRegion)
//    }

}
