//
//  DashboardVC.swift
//  UniBeacon
//
//  Created by Randolph Park on 22/02/2015.
//  Copyright (c) 2015 Randolph Park. All rights reserved.
//

import UIKit

class DashboardVC: UIViewController,UITableViewDelegate {
    
//    let eventList = [ DashboardCellModel(name: "Group Meeting For WWACP", location: "Y406", startTime: "10:20", endTime: "12:20"),
//        DashboardCellModel(name: "Lunch Chat at y block 601  space would be good xD ....(11 words)", location: "Y601", startTime: "12:30", endTime: "14:20"),
//        DashboardCellModel(name: "Gym Booking", location: "P201", startTime: "17:20", endTime: "18:20") ]
    var eventList = [DashboardCellModel]()
    
    @IBOutlet weak var dashboradTable: UITableView!
    
    @IBOutlet weak var spinningWheel: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        println("DashboardVC did load")
        

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.eventList = [DashboardCellModel]()
        self.spinningWheel.hidden = false
        
        let utcTimeZone = NSTimeZone(abbreviation: "UTC")
        let formatter = NSDateFormatter()
        let posIx = NSLocale(localeIdentifier: "en_AU")
        formatter.locale = posIx
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        formatter.timeZone = utcTimeZone
        let now = NSDate()
        let utcTimeString = formatter.stringFromDate(now)
        println("utc time is \(utcTimeString)")
        self.spinningWheel.startAnimating()
        PFCloud.callFunctionInBackground("getEventList", withParameters: ["nowTime" : utcTimeString]) {
            (response : AnyObject!, error : NSError!) -> Void in
            
            if error == nil {
                //println(response)
                for eachEvent in response as! NSArray  {
                    
                    var startTimeString = ""
                    var endTimeString = ""
                    var eventNameString = ""
                    var locationNameString = ""
                    
                    let localDisplayFormatString = "dd/MM-HH:mm"
                    let localDisplayFormatter = NSDateFormatter()
                    localDisplayFormatter.locale = posIx
                    localDisplayFormatter.timeZone = NSTimeZone(name: "Australia/Brisbane")
                    localDisplayFormatter.dateFormat = localDisplayFormatString;
                    
                    //test section 1.2
//                    var testString:NSDate = eachEvent["eventStartTime"] as NSDate
//                    println("test section 1.2 \(localDisplayFormatter.stringFromDate(testString))")
                    
                    if let fetchedStartTime = eachEvent["eventStartTime"] as? NSDate {
                        startTimeString = localDisplayFormatter.stringFromDate(fetchedStartTime)
                    }
                    
                    if let fetchedEndTime = eachEvent["eventEndTime"] as? NSDate {
                        endTimeString = localDisplayFormatter.stringFromDate(fetchedEndTime)
                    }
                    
                    if let fetchedEventName = eachEvent["eventName"] as? String {
                        eventNameString = fetchedEventName
                    }
                    
                    if let fetchedLocationName = eachEvent["locationName"] as? String {
                        locationNameString = fetchedLocationName
                    }
                    
                    let newEventCell = DashboardCellModel(name: eventNameString, location: locationNameString, startTime: startTimeString, endTime: endTimeString)
                    self.eventList.append(newEventCell)
                    
                }
                self.dashboradTable.reloadData()
            }
            self.spinningWheel.stopAnimating()
            self.spinningWheel.hidden = true
            
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Table View Delegte Function
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
//        return 2
    }
    
    func tableView(tableView: UITableView,
        titleForHeaderInSection section: Int)
        -> String {
            if (self.eventList.count == 0) {
                return "No Upcoming Event"
            } else {
                return "Event List"
            }
            
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return eventList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("EventCell", forIndexPath: indexPath)
            as! DashboardCellView
        
        if count(self.eventList) > 0 {
            let event = eventList[indexPath.row] as DashboardCellModel
            
            cell.name.text = event.name
            cell.time.text = event.startTime + " ~ " + event.endTime
            cell.location.text = event.location
        }

        
        return cell
        
    }
    
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        println(nameArray[indexPath.row])
//    }

    
}

